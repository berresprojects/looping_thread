from unittest import TestCase

from mock import MagicMock, patch

from looping_thread import LoopingThread


class TestLoopingThread(TestCase):

    def setUp(self):
        self.looping_thread = LoopingThread()
        self.looping_thread.pre_loop_action = MagicMock()
        self.looping_thread.post_loop_action = MagicMock()
        self.looping_thread.loop_action = MagicMock()

    @patch("looping_thread.sleep")
    def test_run(self, mock_sleep):
        self.looping_thread.is_running = MagicMock()
        self.looping_thread.is_running.side_effect = [True, False]

        self.looping_thread.run()

        self.assertEqual(self.looping_thread.is_running.call_count, 2)
        self.looping_thread.pre_loop_action.assert_called_once()
        mock_sleep.assert_called_once()
        self.looping_thread.loop_action.assert_called_once()
        self.looping_thread.post_loop_action.assert_called_once()

    @patch("looping_thread.sleep")
    def test_run_2(self, mock_sleep):
        self.looping_thread.is_running = MagicMock()
        self.looping_thread.is_running.side_effect = [True, True, True, False]

        self.looping_thread.run()

        self.assertEqual(self.looping_thread.is_running.call_count, 4)
        self.looping_thread.pre_loop_action.assert_called_once()
        self.assertEqual(mock_sleep.call_count, 3)
        self.assertEqual(self.looping_thread.loop_action.call_count, 3)
        self.looping_thread.post_loop_action.assert_called_once()

    def test_destroy(self):
        self.assertTrue(self.looping_thread.is_running())

        self.looping_thread.destroy()

        self.assertFalse(self.looping_thread.is_running())
