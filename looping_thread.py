from threading import Thread as _Thread

from logging import getLogger
from time import sleep

logger = getLogger()


class LoopingThread(_Thread):
    """
    :type self.__running: bool
    """

    def __init__(self):
        super(LoopingThread, self).__init__()
        self.__running = True

    def run(self):
        self.pre_loop_action()
        while self.is_running():
            self.loop_action()
            # yield processor
            sleep(.0001)
        self.post_loop_action()

    def loop_action(self):
        pass

    def is_running(self):
        return self.__running

    def destroy(self):
        logger.info("Destroying {}".format(self))
        self.__running = False

    def pre_loop_action(self):
        pass

    def post_loop_action(self):
        pass
